package com.storage.controller; 

// Generated Sep 5, 2014 10:06:39 AM


import com.storage.service.UsersService;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.storage.*;
import org.springframework.web.bind.annotation.PathVariable;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * Controller object for domain model class Users.
 * @see com.storage.Users
 */

@RestController
@RequestMapping("/storage/Users")
public class StorageUsersController {

    private static final Logger LOGGER = LoggerFactory.getLogger(StorageUsersController.class);

	@Autowired
	@Qualifier("storage.UsersService")
	private UsersService service;

	/**
	 * Processes requests to return lists all available Userss.
	 * 
	 * @param model
	 * @return The name of the  Users list view.
	 */
	 
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public Page<Users> findAll(
			@RequestBody QueryFilter[] queryFilters,
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering Userss list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(queryFilters, pageable);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public Page<Users> getUserss(
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering Userss list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(pageable);
	}
	
	@RequestMapping(value = "/count", method = RequestMethod.GET)
	public Long countAll() {
		LOGGER.debug("counting Userss");
		Long count = service.countAll();
		return count;
	}


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    	public Users getUsers(@PathVariable("id") Integer id) throws EntityNotFoundException {
    		LOGGER.debug("Getting Users with id: {}" , id);
    		Users instance = service.findById(id);
    		LOGGER.debug("Users details with id: {}" , instance);
    		return instance;
    	}

    	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    	public boolean delete(@PathVariable("id") Integer id) throws EntityNotFoundException {
    		LOGGER.debug("Deleting Users with id: {}" , id);
    		Users deleted = service.delete(id);
    		return deleted != null;
    	}

    	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    	public Users editUsers(@PathVariable("id") Integer id, @RequestBody Users instance) throws EntityNotFoundException {
            LOGGER.debug("Editing Users with id: {}" , instance.getUserId());
            instance.setUserId(id);
    		instance = service.update(instance);
    		LOGGER.debug("Users details with id: {}" , instance);
    		return instance;
    	}



	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Users createUsers(@RequestBody Users instance) {
		LOGGER.debug("Create Users with information: {}" , instance);
		instance = service.create(instance);
		LOGGER.debug("Created Users with information: {}" , instance);
	    return instance;
	}
	
	/**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
	protected void setUsersService(UsersService service) {
		this.service = service;
	}
}

