package com.storage.repository; 
// Generated Sep 5, 2014 10:06:39 AM 

import com.wavemaker.runtime.data.dao.WMGenericDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.storage.*;
/**
 * Specifies methods used to obtain and modify Users related information
 * which is stored in the database.
 */
@Repository("storage.UsersDao")
public class UsersRepository extends WMGenericDaoImpl <Users, Integer> {

   @Autowired
   @Qualifier("storageTemplate")
   private HibernateTemplate template;

   public HibernateTemplate getTemplate() {
        return this.template;
   }
}

