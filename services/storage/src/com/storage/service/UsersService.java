package com.storage.service;
// Generated Sep 5, 2014 10:06:39 AM


import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.storage.*;
/**
 * Service object for domain model class Users.
 * @see com.storage.Users
 */

public interface UsersService {

   /**
	 * Creates a new users.
	 * 
	 * @param created
	 *            The information of the created users.
	 * @return The created users.
	 */
	public Users create(Users created);

	/**
	 * Deletes a users.
	 * 
	 * @param usersId
	 *            The id of the deleted users.
	 * @return The deleted users.
	 * @throws EntityNotFoundException
	 *             if no users is found with the given id.
	 */
	public Users delete(Integer usersId) throws EntityNotFoundException;

	/**
	 * Finds all userss.
	 * 
	 * @return A list of userss.
	 */
	public Page<Users> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<Users> findAll(Pageable pageable);
	
	/**
	 * Finds users by id.
	 * 
	 * @param id
	 *            The id of the wanted users.
	 * @return The found users. If no users is found, this method returns
	 *         null.
	 */
	public Users findById(Integer id) throws EntityNotFoundException;

	/**
	 * Updates the information of a users.
	 * 
	 * @param updated
	 *            The information of the updated users.
	 * @return The updated users.
	 * @throws EntityNotFoundException
	 *             if no users is found with given id.
	 */
	public Users update(Users updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the userss in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the users.
	 */

	public long countAll();

}

