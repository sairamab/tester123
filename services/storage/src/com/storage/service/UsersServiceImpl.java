package com.storage.service;
// Generated Sep 5, 2014 10:06:39 AM

import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.storage.*;
import com.wavemaker.runtime.data.dao.*;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * ServiceImpl object for domain model class Users.
 * @see com.storage.Users
 */
@Service("storage.UsersService")
public class UsersServiceImpl implements UsersService {


    private static final Logger LOGGER = LoggerFactory.getLogger(UsersServiceImpl.class);


@Autowired
@Qualifier("storage.UsersDao")
private WMGenericDao<Users, Integer> wmGenericDao;
  public void setWMGenericDao(WMGenericDao<Users, Integer> wmGenericDao){
          this.wmGenericDao = wmGenericDao;
  }

    @Transactional(value = "storageTransactionManager")
    @Override
    public Users create(Users users) {
        LOGGER.debug("Creating a new users with information: {}" , users);
        return this.wmGenericDao.create(users);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "storageTransactionManager")
    @Override
    public Users delete(Integer usersId) throws EntityNotFoundException {
        LOGGER.debug("Deleting users with id: {}" , usersId);
        Users deleted = this.wmGenericDao.findById(usersId);
        if (deleted == null) {
            LOGGER.debug("No users found with id: {}" , usersId);
            throw new EntityNotFoundException(String.valueOf(usersId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true, value = "storageTransactionManager")
    @Override
    public Page<Users> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all userss");
        return this.wmGenericDao.search(queryFilters, pageable);
    }
    
    @Transactional(readOnly = true, value = "storageTransactionManager")
    @Override
    public Page<Users> findAll(Pageable pageable) {
        LOGGER.debug("Finding all userss");
        return this.wmGenericDao.search(null, pageable);
    }

    @Transactional(readOnly = true, value = "storageTransactionManager")
    @Override
    public Users findById(Integer id) throws EntityNotFoundException {
        LOGGER.debug("Finding users by id: {}" , id);
        Users users=this.wmGenericDao.findById(id);
        if(users==null){
            LOGGER.debug("No users found with id: {}" , id);
            throw new EntityNotFoundException(String.valueOf(id));
        }
        return users;
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "storageTransactionManager")
    @Override
    public Users update(Users updated) throws EntityNotFoundException {
        LOGGER.debug("Updating users with information: {}" , updated);
        this.wmGenericDao.update(updated);
        return this.wmGenericDao.findById((Integer)updated.getUserId());
    }

    @Transactional(readOnly = true, value = "storageTransactionManager")
    @Override
    public long countAll() {
        return this.wmGenericDao.count();
    }
}


